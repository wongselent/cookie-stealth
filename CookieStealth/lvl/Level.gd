extends Node2D

export var daytime = false
export (String) var level_name = "Default Name" 

onready var nav_2d = $Navigation2D
onready var player = $Player

func _ready():
	randomize()
	
	$Bedroom.monitoring = false
#	set_camera_limits()
	
	for s in get_tree().get_nodes_in_group("siblings"):
		s.daytime = daytime
	
	if daytime:
		VisualServer.set_default_clear_color(Color(0.3,0.4,0.6,1.0))
		$CanvasModulate.color = Color.white
	else:
		VisualServer.set_default_clear_color(Color(0.0,0.1,0.2,1.0))
		$CanvasModulate.color = Color.darkgray
	
	$Player/Light2D.visible = !daytime
	
#	$Player.target($Cookie.global_position)
	
	$CanvasLayer/Fade.fade_in()

func _process(delta):
	var player_spotted = false
	for s in get_tree().get_nodes_in_group("siblings"):
		if s.state == s.State.FOLLOWING:
			player_spotted = true
			break
	
	$Player.spotted = player_spotted
	
	# for testing
#	if Input.is_action_just_pressed("interact2"):
#		get_tree().call_group("siblings", "alert", get_global_mouse_position(), 1000000)

func set_camera_limits():
	var map_limits = $Ground.get_used_rect()
	var map_cell_size = $Ground.cell_size
	
	$Player/Cursor/Camera2D.limit_left = map_limits.position.x * map_cell_size.x
	$Player/Cursor/Camera2D.limit_top = map_limits.position.y * map_cell_size.y
	$Player/Cursor/Camera2D.limit_right = map_limits.end.x * map_cell_size.x
	$Player/Cursor/Camera2D.limit_bottom = map_limits.end.y * map_cell_size.y

func get_nav_path(pos1, pos2):
	return nav_2d.get_simple_path(pos1, pos2)

func _on_Bedroom_body_entered(body):
	if body.name == "Player" && !player.spotted:
		win()

func disable_messages():
	for m in get_tree().get_nodes_in_group("messages"):
		m.queue_free()

func defeat():
	$CanvasLayer/Audio/Caught.play()
	
	var msg = "gotcha, you rascal!"
	
	if $Player.cookie:
		msg = "nice try."
	
	$CanvasLayer/CaughtScreen.flash(msg)

func win():
	$Timer.paused = true
	
	$CanvasLayer/WinScreen.set_score($Player.spot_count, stepify($Timer.wait_time - $Timer.time_left, 0.001))
	
	$CanvasLayer/Audio/Win.play()
	
	$CanvasLayer/WinScreen.activate(level_name)

func _on_Fade_fade_in_finished():
	$Timer.start()
