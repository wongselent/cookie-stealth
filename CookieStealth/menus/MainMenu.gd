extends Control

var cmd

func _ready():
	$Fade.fade_in()
	
	VisualServer.set_default_clear_color(Color(0.13,0.2,0.23,1.0))
	
	for button in $Menu/HBoxContainer/MarginContainer/Buttons.get_children():
		button.connect("pressed", self, "_on_Button_pressed", [button.target_scene_path])

func _on_Button_pressed(path):
	cmd = path
	
	$Fade.fade_out()

func _on_Fade_fade_finished():
	if cmd == "quit":
		get_tree().quit()
	else:
		get_tree().change_scene(cmd)
