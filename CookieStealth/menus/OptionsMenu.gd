extends Control

func _ready():
	set_fullscreen_check()
	set_mute_check()
	
	$Fade.fade_in()

func _on_MenuButton_pressed():
	$Fade.fade_out()

func _on_Fade_fade_finished():
	get_tree().change_scene($CenterContainer/VBoxContainer/MenuButton.target_scene_path)

func _on_Fullscreen_pressed():
	OS.window_fullscreen = !OS.window_fullscreen
	set_fullscreen_check()

func set_fullscreen_check():
	$CenterContainer/VBoxContainer/Fullscreen.text = get_check("fullscreen", OS.window_fullscreen)

func _on_Mute_pressed():
	AudioServer.set_bus_mute(0, !AudioServer.is_bus_mute(0))
	set_mute_check()

func set_mute_check():
	$CenterContainer/VBoxContainer/Mute.text = get_check("mute", AudioServer.is_bus_mute(0))

func get_check(label, boolean):
	if boolean:
		return "[x] " + label
	else:
		return "[ ] " + label