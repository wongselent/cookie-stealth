extends Control

var cmd

func _input(event):
	if event.is_action_pressed("pause"):
		if get_tree().paused:
			resume()
		else:
			pause()

func _on_Resume_pressed():
	resume()

func _on_Exit_pressed():
	cmd = $CenterContainer/VBoxContainer/Exit.target_scene_path
	fade_quit()

func _on_Restart_pressed():
	cmd = "reset"
	fade_quit()

func fade_quit():
	$ColorRect2.show()
	$AnimationPlayer.play("quit")

func pause():
	show()
	$AnimationPlayer.play("in")
	get_tree().paused = true

func resume():
	$AnimationPlayer.play("out")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "out":
		get_tree().paused = false
		hide()
	elif anim_name == "quit":
		get_tree().paused = false
		if cmd == "reset":
			get_tree().reload_current_scene()
		else:
			get_tree().change_scene(cmd)
