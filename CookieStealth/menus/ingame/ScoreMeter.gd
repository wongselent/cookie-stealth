extends Control

func set_score(ghost, time):
	if ghost == 0:
		$VBoxContainer/Ghost.text = "*GHOST*"
	elif ghost == 1:
		$VBoxContainer/Ghost.text = "detected once."
	else:
		$VBoxContainer/Ghost.text = "detected " + str(ghost) + " times."
	
	var m = int(time / 60)
	
	var s = time - m * 60
	
	var text = "time: "
	
	if m > 0:
		text += str(m) + "m"
	
	text += str(s) + "s"
	
	$VBoxContainer/Time.text = text
