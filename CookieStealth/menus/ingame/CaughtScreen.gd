extends Control

func flash(msg):
	get_tree().paused = true
	
	$CenterContainer/MenuLabel.text = msg
	
	show()
	$AnimationPlayer.play("fade")

func _on_AnimationPlayer_animation_finished(anim_name):
	get_tree().paused = false
	get_tree().reload_current_scene()
