extends Control

var cmd

func _ready():
	$CenterContainer/VBoxContainer/Replay.connect("pressed", self, "_on_Button_pressed", [$CenterContainer/VBoxContainer/Replay.target_scene_path])
	$CenterContainer/VBoxContainer/Exit.connect("pressed", self, "_on_Button_pressed", [$CenterContainer/VBoxContainer/Exit.target_scene_path])

func activate(level_name):
	$CenterContainer/VBoxContainer/Label.text = level_name
	show()
	get_tree().paused = true
	$AnimationPlayer.play("in")

func _on_Button_pressed(path):
	cmd = path
	$ColorRect2.show()
	$AnimationPlayer.play("out")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name != "out":
		return
	
	get_tree().paused = false
	if cmd == $CenterContainer/VBoxContainer/Replay.target_scene_path:
		get_tree().reload_current_scene()
	else:
		get_tree().change_scene(cmd)

func set_score(ghost, time):
	$CenterContainer/VBoxContainer/ScoreMeter.set_score(ghost, time)