extends Area2D

export (String, MULTILINE) var text

func _ready():
	add_to_group("messages")
	
	$CanvasLayer/CenterContainer.hide()
	$CanvasLayer/CenterContainer/MarginContainer/Label.text = text

func _on_MessageBox_body_entered(body):
	if body.name == "Player":
		$CanvasLayer/CenterContainer.show()
		$CanvasLayer/CenterContainer/MarginContainer/Label/AnimationPlayer.play("in")

func _on_MessageBox_body_exited(body):
	if body.name == "Player":
		$CanvasLayer/CenterContainer/MarginContainer/Label/AnimationPlayer.play("out")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "out":
		$CanvasLayer/CenterContainer.hide()
