extends Control

var cmd

func _ready():
	VisualServer.set_default_clear_color(Color(0.13,0.2,0.23,1.0))
	
	$Fade.fade_in()
	
	for button in $CenterContainer/Buttons.get_children():
		if !(button is Button):
			continue
		button.connect("pressed", self, "_on_Button_pressed", [button.target_scene_path])

func _on_Button_pressed(path):
	$Fade.fade_out()
	
	cmd = path

func _on_Fade_fade_finished():
	get_tree().change_scene(cmd)
