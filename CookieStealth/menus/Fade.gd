extends ColorRect

signal fade_finished
signal fade_in_finished

func fade_out():
	show()
	$AnimationPlayer.play("fade_out")

func fade_in():
	show()
	$AnimationPlayer.play("fade_in")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "fade_out":
		emit_signal("fade_finished")
	else:
		hide()
		emit_signal("fade_in_finished")
