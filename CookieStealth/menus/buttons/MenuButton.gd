extends Button

export (String, MULTILINE) var target_scene_path

func _on_MenuButton_button_down():
	$ClickOn.play()

func _on_MenuButton_button_up():
	$ClickOff.play()
