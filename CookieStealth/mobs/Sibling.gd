tool

extends KinematicBody2D

export (Texture) var sprite setget sprite_changed
export (Texture) var angry_sprite

export (int) var angle setget angle_changed

const MOVE_SPEED = 180
const IDLE_SPEED = 100
const TURN_SPEED = 10
const STOP_THRESHOLD = IDLE_SPEED / 4.0

const IDLE_FREEZE_TIME = 1 # seconds

# random angle deviation when bouncing off walls during idle
const BOUNCE_DEVIATION = 70

const DETECT_RADIUS = 300
const DETECT_FOV = 100
const SENSE_RADIUS = 58

onready var raycast = $Sprite/RayCast2D
onready var raycast2 = $Sprite/RayCast2D2

onready var radius = $CollisionShape2D.shape.radius

var player = null
var path : PoolVector2Array = []

var look_vector = Vector2()
var turning = false

var freeze_time = 0

var daytime

enum State {
	PATROLLING,
	IDLE,
	FOLLOWING,
	SEARCHING
}

var state = State.PATROLLING

func _ready():
	add_to_group("siblings")
	
	$Sprite.global_rotation = deg2rad(angle)
	look_vector = Vector2(1, 0).rotated($Sprite.global_rotation)
	
	set_sprites()
	
	$Particles2D.emitting = false
	$Particles2D.one_shot = true

func _process(delta):
	if Engine.editor_hint:
		return
	
	if player == null || (daytime && !player.cookie):
		path = []
		return
	
	if freeze_time > 0:
		freeze_time -= delta
	else:
		freeze_time = 0
	
	if path.size() > 0 && $Sprite.texture != angry_sprite:
		$Sprite.set_texture(angry_sprite)
	
	if player_in_view():
		freeze_time = 0
		
		state = State.FOLLOWING
		
		detect(player.global_position)
		
		if !player.spotted && !$AlertSound.playing:
			$Particles2D.emitting = true
			$AlertSound.play()
			
			player.spot_count += 1
	elif path.size() > 0:
#		if state == State.FOLLOWING:
#			# do stuff when player goes out of view
		
		state = State.SEARCHING
		
		# has reached path node (back to idle/next path node)
		if (path[0] - global_position).length() < radius:
			path.remove(0)
	elif state != State.PATROLLING:
		
		if state == State.FOLLOWING || state == State.SEARCHING:
			# freeze before going idle
			freeze(IDLE_FREEZE_TIME)
			# set look_vector from sprite rotation
			set_look_from_rot()
		
		if $Sprite.texture != sprite:
			$Sprite.set_texture(sprite)
		state = State.IDLE

func _physics_process(delta):
	if player == null || freeze_time > 0 || Engine.editor_hint:
		return
	
	match state:
		State.PATROLLING:
			patrol(delta)
		State.IDLE:
			idle(delta)
		State.SEARCHING:
			search(delta)
		State.FOLLOWING:
			follow_player(delta)

func patrol(delta):
	if !turning && (raycast.is_colliding() || raycast2.is_colliding()):
		look_vector = -look_vector
	
	turning = turn_to(global_position + look_vector, TURN_SPEED / 2.0, delta, false)
	
	if !turning:
		move_and_slide($Sprite.transform.x * IDLE_SPEED)

func idle(delta):
	$RunningParticles.emitting = false
	
	if !turning:
		var normal
		
		if raycast.is_colliding():
			normal = raycast.get_collision_normal()
		elif raycast2.is_colliding():
			normal = raycast2.get_collision_normal()
		
		if normal != null:
			look_vector = Vector2(1, 0).rotated(normal.angle() + rand_range(deg2rad(-BOUNCE_DEVIATION), deg2rad(BOUNCE_DEVIATION)))
	
	turning = turn_to(global_position + look_vector, TURN_SPEED * 0.75, delta, true)
	
	if !turning:
		move_and_slide($Sprite.transform.x * IDLE_SPEED)

func search(delta):
	$RunningParticles.emitting = true
	
	if path.size() > 0:
		turn_to(path[0], TURN_SPEED, delta, true)
	
	var velocity = move_and_slide($Sprite.transform.x * MOVE_SPEED)
	
	open_doors()
	
	if velocity.length() < STOP_THRESHOLD:
		path = []

func follow_player(delta):
	$RunningParticles.emitting = true
	
	turn_to(player.global_position, TURN_SPEED, delta, true)
	
	move_and_slide($Sprite.transform.x * MOVE_SPEED)
	
	if raycast.is_colliding():
		var collider = raycast.get_collider()
		if collider.name == "Player":
			collider.kill()
			return
	
	if raycast2.is_colliding():
		var collider = raycast2.get_collider()
		if collider.name == "Player":
			collider.kill()

func player_in_view():
	if global_position.distance_to(player.global_position) < SENSE_RADIUS:
		return true
	
	var vec_to_player = (player.global_position - global_position).normalized()
	
	if global_position.distance_to(player.global_position) < DETECT_RADIUS:
		var space = get_world_2d().direct_space_state
		var ray = space.intersect_ray(global_position, player.global_position, [self], collision_mask)
		
		if ray && ray.collider.name == "Player":
			var angle_to_player = abs(rad2deg($Sprite.transform.x.angle_to(vec_to_player)))
			return angle_to_player < DETECT_FOV / 2
	
	return false

func turn_to(pos, speed, delta, use_vectors):
	var before_angle = $Sprite.global_rotation
	
	if use_vectors:
		$Sprite.global_rotation = $Sprite.transform.x.linear_interpolate((pos - global_position).normalized(), speed * delta).angle()
	else:
		$Sprite.global_rotation = lerp($Sprite.global_rotation, (pos - global_position).angle(), speed * delta)
	
	var da = $Sprite.global_rotation - before_angle
	var b = abs(da) > deg2rad(0.1)
	
	# return if turning
	return b

func set_look_from_rot():
	look_vector = Vector2(1, 0).rotated($Sprite.global_rotation)

func open_doors():
	if raycast.is_colliding():
		if raycast.get_collider().is_in_group("doors"):
			raycast.get_collider().interact(global_position)

func alert(pos, radius):
	if player == null || (daytime && !player.cookie):
		path = []
		return
	
	if state != State.FOLLOWING && global_position.distance_to(pos) < radius:
		if path.size() == 0:
			$Particles2D.emitting = true
		
		path = get_parent().get_parent().get_nav_path(global_position, pos)

func detect(pos):
	if player == null || (daytime && !player.cookie):
		path = []
		return
	
	path = [pos]

func freeze(sec):
	$RunningParticles.emitting = false
	freeze_time = sec

func set_player(p):
	player = p

func set_sprites():
	$Sprite.set_texture(sprite)

func sprite_changed(s):
	sprite = s
	
	if Engine.editor_hint:
		$Sprite.texture = s

func angle_changed(a):
	angle = a
	
	if Engine.editor_hint:
		$Sprite.global_rotation = deg2rad(a)
