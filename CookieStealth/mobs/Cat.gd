extends KinematicBody2D

const MOVE_SPEED = 300
const IDLE_SPEED = 80
const TURN_SPEED = 10

const STOP_RADIUS = 48

onready var raycast = $Sprite/RayCast2D
onready var walk_raycast = $Sprite/WalkRaycast

var ball = null

enum State {
	IDLE,
	FOLLOWING,
}

var state = State.IDLE

func _ready():
	add_to_group("cats")

func _process(delta):
	ball = get_ball_in_view()
	
	if ball == null:
		state = State.IDLE
	else:
		if state == State.IDLE:
			$Meow.play()
		
		state = State.FOLLOWING

func _physics_process(delta):
	match state:
		State.IDLE:
			idle(delta)
		State.FOLLOWING:
			follow_ball(delta)

func idle(delta):
	$RunningParticles.emitting = false
	var normal
	
	if walk_raycast.is_colliding():
		normal = walk_raycast.get_collision_normal()
	
	if normal != null:
		$Sprite.global_rotation = normal.angle() + deg2rad(90)
	
	move_and_slide($Sprite.transform.x * IDLE_SPEED)

func follow_ball(delta):
	$RunningParticles.emitting = true
	
	$Sprite.global_rotation = $Sprite.transform.x.linear_interpolate((ball.global_position - global_position).normalized(),
		TURN_SPEED * delta).angle()
	
	if global_position.distance_to(ball.global_position) > STOP_RADIUS:
		move_and_slide($Sprite.transform.x * MOVE_SPEED)
	else:
		# cat got ball stuck, ball stops moving
		ball.velocity = Vector2()
		return
	
	if raycast.is_colliding():
		var collider = raycast.get_collider()
		if collider.is_in_group("balls"):
			collider.interact(global_position)
			return

func get_ball_in_view():
	var space = get_world_2d().direct_space_state
	
	for ball in get_tree().get_nodes_in_group("balls"):
		# if ball not moving very fast, ignore
		if ball.velocity.length() < ball.THROW_POWER / 10.0:
			continue
		
		var vec_to_ball = (ball.global_position - global_position).normalized()
		var ray = space.intersect_ray(global_position, ball.global_position, [self], collision_mask)
		
		if ray && ray.collider.is_in_group("balls"):
			return ball
	
	return null

func interact(pos):
	$Purr.play()
