extends KinematicBody2D

const MOVE_SPEED = 150
const RUN_SPEED = 280

const RUN_ALERT_RADIUS = 350

const LOOK_DEADZONE = 20

var time = 0

onready var raycast = $Sprite/RayCast2D

onready var radius = $CollisionShape2D.shape.radius

var spotted = false
var spot_count = 0

var steps = 0

var cookie = false

var controller = false

func _ready():
	yield(get_tree(), "idle_frame")

	get_tree().call_group("siblings", "set_player", self)

func _process(delta):
	step()

	# screen shake
	if spotted:
		time += delta
		$Cursor.global_rotation = sin(time * 50) / 130.0
	else:
		if time > 0:
			time = 0
		$Cursor.global_rotation = lerp($Cursor.global_rotation, 0, 0.15)

func _physics_process(delta):
	var velocity = Vector2()
	if Input.is_action_pressed("move_up"):
		velocity.y -= 1
	if Input.is_action_pressed("move_down"):
		velocity.y += 1
	if Input.is_action_pressed("move_left"):
		velocity.x -= 1
	if Input.is_action_pressed("move_right"):
		velocity.x += 1

	var speed = MOVE_SPEED

	if Input.is_action_pressed("run"):
		speed = RUN_SPEED

	var move = move_and_slide(velocity.normalized() * speed)

	steps = 0
	if move.length() > MOVE_SPEED:
		steps = 2
		$RunningParticles.emitting = true
		get_tree().call_group("siblings", "alert", global_position, RUN_ALERT_RADIUS)
	else:
		$RunningParticles.emitting = false
		if move.length() > MOVE_SPEED / 2.0:
			steps = 1

	get_look()

	if Input.is_action_just_pressed("interact"):
		var collider = raycast.get_collider()
		if raycast.is_colliding():
			if collider.has_method("interact"):
				collider.interact(global_position)

	if Input.is_action_just_pressed("interact2"):
		var collider = raycast.get_collider()
		if raycast.is_colliding():
			if collider.has_method("interact2"):
				collider.interact2(global_position)
			elif collider.has_method("interact"):
				collider.interact(global_position)

func get_look():
	if controller:
		var xaxis = Input.get_action_strength("look_right") - Input.get_action_strength("look_left")
		var yaxis = Input.get_action_strength("look_down") - Input.get_action_strength("look_up")

		var look = Vector2(xaxis, yaxis) * 100

		if look.length() < LOOK_DEADZONE:
			return

		$Sprite.global_rotation = atan2(look.y, look.x)
	else:
		var look = get_global_mouse_position() - global_position
		$Sprite.global_rotation = atan2(look.y, look.x)

func step():
	match steps:
		0:
			return
		1:
			if !$Walk.playing:
				$Walk.volume_db = rand_range(-16, -8)
				$Walk.play()
		2:
			if !$Run.playing:
				$Run.volume_db = rand_range(-4, -2)
				$Run.play()

func toggle_collision(a):
	$CollisionShape2D.disabled = !a

func kill():
	get_parent().defeat()
