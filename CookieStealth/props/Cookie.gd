extends Area2D

func _ready():
	$PickupSparkle.emitting = false

func _on_Cookie_body_entered(body):
	if body.name == "Player":
		body.cookie = true
		get_node("../Bedroom").monitoring = true
		get_parent().disable_messages()
		pickup()

func pickup():
	if $Timer.time_left > 0:
		return
	
	$Pickup.play()
	$Sprite.hide()
	$CollisionShape2D.disabled = true
	monitoring = false
	$Sparkle.emitting = false
	$PickupSparkle.emitting = true
	$Timer.start()

func _on_Timer_timeout():
	queue_free()
