extends StaticBody2D

export var smooth = 10

# between 0 and 1, how much the door must be open/closed to be used again
const INTERACT_THRESHOLD = 0.99

var open = false

var target_rotation = 0

func _ready():
	add_to_group("doors")

func _physics_process(delta):
	rotation = lerp(rotation, target_rotation, smooth * delta)

func interact(pos):
	# if not at least half opened/closed, do nothing
	if abs(target_rotation - rotation) > deg2rad((1 - INTERACT_THRESHOLD) * 90):
		return
	
	if open:
		$Close.play()
		
		target_rotation = 0
	else:
		$Open.play()
		
		var door_angle = round(fposmod(rad2deg(get_parent().global_rotation), 360))
		
		target_rotation = deg2rad(90)
		
		match door_angle:
			0.0:
				if pos.y > global_position.y:
					target_rotation = -deg2rad(90)
			90.0:
				if pos.x < global_position.x:
					target_rotation = -deg2rad(90)
			180.0:
				if pos.y < global_position.y:
					target_rotation = -deg2rad(90)
			270.0:
				if pos.x > global_position.x:
					target_rotation = -deg2rad(90)
	open = !open

func interact2(pos):
	interact(pos + (global_position - pos) * 2)