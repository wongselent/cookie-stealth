extends StaticBody2D

const LERP_POWER = 0.2

var player = null

var reset_pos = Vector2()

func _physics_process(delta):
	if player == null:
		return
	
	player.global_position = lerp(player.global_position, global_position, LERP_POWER)

func interact(pos):
	if player == null:
		reset_pos = pos
		player = get_node("../../Player")
		
		player.toggle_collision(false)
		z_index = 1
	else:
		player.global_position = reset_pos
		player.toggle_collision(true)
		z_index = 0
		
		player = null
