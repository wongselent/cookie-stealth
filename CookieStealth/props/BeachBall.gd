extends KinematicBody2D

var velocity = Vector2()

const DAMPING = 0.99
const BOUNCE_DAMPING = 0.9

const THROW_POWER = 450
const ALERT_RADIUS = 450

onready var pickup_radius = $Area2D/CollisionShape2D.shape.radius

var player = null

func _ready():
	add_to_group("balls")
	
	$Sprite.global_rotation = rand_range(0, 2*PI)

func _physics_process(delta):
	if player:
		$CollisionShape2D.disabled = true
		
		var rot = player.get_node("Sprite").global_rotation
		var target_pos = player.global_position + Vector2(player.radius + pickup_radius, 0).rotated(rot)
		
		var smoothing_speed = 25
		
		global_position = global_position.linear_interpolate(target_pos, smoothing_speed * delta)
		$Sprite.global_rotation = rot
	else:
		velocity *= DAMPING
		
		$Sprite.global_rotation += velocity.length() / 50.0 * delta
		
		if velocity.length() < 5:
			velocity = Vector2()
		
		var collision = move_and_collide(velocity * delta)
		
		if collision:
			play_bounce()
			
			velocity = velocity.bounce(collision.normal) * BOUNCE_DAMPING
			
			if velocity.length() > THROW_POWER / 4.0 && collision.collider.name != "Player":
				get_tree().call_group("siblings", "alert", global_position, ALERT_RADIUS)

func _on_Area2D_body_entered(body):
	if player == null && velocity.length() < THROW_POWER / 2.0 && body.name == "Player":
		$CollisionShape2D.disabled = true
		
		player = body

func interact(pos):
	if player == null:
		play_bounce()
	
	player = null
	
	$CollisionShape2D.disabled = false
	
	velocity = (global_position - pos).normalized() * THROW_POWER

func interact2(pos):
	if player == null:
		return
	
	player = null
	$CollisionShape2D.disabled = false
	
	velocity = (global_position - pos).normalized() * THROW_POWER / 50.0

func play_bounce():
	if velocity.length() < 1:
		return
	
	$AnimationPlayer.play("bounce")
	
	$Bounce.volume_db = (velocity.length() - THROW_POWER) / 50.0 - 10
	$Bounce.play(0.0)
