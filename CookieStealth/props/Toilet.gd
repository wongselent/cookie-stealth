extends StaticBody2D

const RADIUS = 500

func interact(pos):
	$Flush.play()
	
	get_tree().call_group("siblings", "alert", global_position, RADIUS)
