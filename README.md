# Cookie Stealth

This is a top-down stealth game made with the [Godot](https://godotengine.org/) game engine.
This is experimental, being my first full game made in Godot.

# Download

Latest build can be found [here](https://heraldod.itch.io/cookie-stealth) (Windows, Linux/X11 64bit and Mac OSX).

# Gameplay

You are a little boy. Your goal is to steal a cookie hidden somewhere (usually a kitchen) and bring it back to your room. Avoid being seen by parents, siblings and classmates. Use various tools in your environment to distract them or to hide.
This game plays sort of like hotline miami, minus the blood and guts (sadly).

# Screenshots
<details><summary>Click to show</summary>
<p>

![Image missing](screenshot1.png)

![Image missing](screenshot2.png)

</p>
</details>

# Installation

To play, download the executable for your OS along with the .pck file, and place them in the same directory. Then, simply run the executable to launch the game.
